# Polyakov Dmitry`s GNU+Linux Dotfiles
##### _ArchLinux Edition_
---

## Packages:
+ ### **bspwm** + sxhkd
    Initially I used i3 along with i3 bar, then started using polybar, and after that I moved to bspwm. The architecture and working principle of bspwm in my opinion is much better than that in i3.

+ ### **kitty** + bash
    Kitty some slower than st but better in features. I tried to use zsh only as an interactive shell, but I do not understand what his chip and returned back to bash. Better to learn fish later somehow. Font is Anonymous Pro.

+ ### **mpd** + ncmpcpp + mpc
    Initially I used cmus as it was easier to configure, but then I became convinced of the convenience of a modular architecture, a flexible tag system and integration with different frontends.

+ ### **polybar** + awesome-font
    By and large, this is a standard config without useless characters and a module for displaying updates. The use of polybar has largely contributed to the switch to bspwm.

+ ### **neovim** with Luke configuration
    For a long time I used the default vim, but recently tried to use neovim and was surprised by the increase in speed. How much I lost D: Luke`s config is excellent!

+ Thunar, dunst, sxiv, xwallpaper
+ *And many scripts for comfy workflow*

## Install
I use Stow for dotfile managment
```bash
git clone git@gitlab.com:LukeSmithFanBoy/dotfiles.git
cd dotfiles
stow *
sudo pacman -S --needed $(comm -12 <(pacman -Slq|sort) <(sort pacman.list) )
yay -S --noedit --noconfirm --needed aur.list
```

## Special thanks to
[Richard Stallman](https://www.stallman.org/)
[Luke Smith](https://github.com/LukeSmithxyz)
[Derek Taylor](https://gitlab.com/dwt1)
[Wolfgang](https://github.com/notthebee)
