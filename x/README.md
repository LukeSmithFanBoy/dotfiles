# Dmitry Polyakov's GNU+Linux Dotfiles
##### _ArchLinux Edition_
---

[](https://imgur.com/9CkmguG)
## Packages:
+ ### **bspwm** + sxhkd
    Initially I used i3 along with i3 bar, then started using polybar, and after that I moved to bspwm. The architecture and working principle of bspwm in my opinion is much better than that in i3.

+ ### **kitty** + bash
    Kitty some slower than st but better in features. I tried to use zsh only as an interactive shell, but I do not understand what his pluses and returned back to bash. Better to learn fish later somehow. Font is Anonymous Pro.

+ ### **mpd** + ncmpcpp + mpc
    Initially I used cmus as it was easier to configure, but then I became convinced of the convenience of a modular architecture, a flexible tag system and integration with different frontends.

+ ### **polybar** + awesome-font
    By and large, this is a standard config without useless characters and with module for displaying updates. The use of polybar has largely contributed to the switch to bspwm.
+ ### **neovim** with Luke configuration
    For a long time I used the default vim, but recently tried to use neovim and was surprised by the increase in speed. How much I lost D: Luke`s config is excellent!

+ Thunar(nnn), dunst, sxiv, xwallpaper
+ *And many scripts for comfy workflow*

## TODO:
+ Swith to VoidLinux
+ Use nnn as privary file manager

## Special thanks to
[Richard Stallman](https://www.stallman.org/)
[Luke Smith](https://github.com/LukeSmithxyz)
[Derek Taylor](https://gitlab.com/dwt1)
[Wolfgang](https://github.com/notthebee)
