# Setup fzf
# ---------

way_to_fzf="$HOME/.config/shell/fzf"
if [[ ! "$PATH" == "$way_to_fzf"/bin* ]]; then
  export PATH="${PATH:+${PATH}:}"$way_to_fzf"/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source ""$way_to_fzf"/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source ""$way_to_fzf"/shell/key-bindings.bash"
