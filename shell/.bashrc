#
# BASHRC
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# Append to the history file, don't overwrite it
shopt -s histappend
# Check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize
# Correct minor errors in the spelling of a directory component in a cd command
shopt -s cdspell
# Save all lines of a multiple-line command in the same history entry (allows easy re-editing of multi-line commands)
shopt -s cmdhist
# Infinite history
HISTSIZE= HISTFILESIZE=

# Load git prompt
[ -r /usr/share/git/completion/git-prompt.sh ] && source /usr/share/git/completion/git-prompt.sh
#
# Change prompt to:
# [user@hostname] ~ (branch)
#   
#
export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput dim)\]\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h\[$(tput sgr0)\]\[$(tput bold)\]\[$(tput setaf 1)\]] \[$(tput setaf 5)\\w\[$(tput setaf 7)\]\$(__git_ps1)\n   \[$(tput sgr0)\]"

# GPG over SSH agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi

export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null

# Load aliases
[ -f $HOME/.config/shell/aliasrc ] && source $HOME/.config/shell/aliasrc
# Load fzf features
[ -f $HOME/.config/shell/fzf.bash ] && source $HOME/.config/shell/fzf.bash

